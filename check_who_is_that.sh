#!/bin/bash


is_ip_valid()
{
  local temp
  local error
  
  for (( i = 1; i < 5; i++ ))
    do
    temp=$(echo "$ip" | cut -f$i -d".")
    echo $temp
    if [ "$temp" -ge 1 -a "$temp" -le 255 ]; then valid_ip="true";
     
    else
      echo "Wrong IP adress format"
      exit 1
    fi
    done
}

function who_are_you()
{
api_response=$( curl -s http://ip-api.com/json/$ip?fields=city,org,as )
originAS=$(echo $api_response | jq -r '.as' | awk '{print $1}' )
city=$(echo $api_response | jq -r '.city' )
organisation=$(echo $api_response | jq -r '.org' )

}


    n=1
for arg in "$@"; do
 

  if [[ "$arg" =~ ^-.* ]]; then

    case $arg in
      -c)
        param_flag="csv";;
      -csv)
        param_flag="csv";;
      -h)
        param_flag="help";;
      --help)
        param_flag="help";;
      *)
        echo "this parameter can not be handled";;
    esac
  elif [[ $arg =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]];then

    ip_provided="1"
    ip=$arg
    is_ip_valid
  else
    echo -e "Wrong!\nUse -h or --help to see the syntax"
    exit 1  
  fi

done

if [ "$param_flag" == "help" ]; then
  echo -e "\n#################################"
  echo -e "-c or -csv \t returns data in csv format with ; as delimiter"
  echo -e "-h or --help \t returns usage guide"
  echo -e "\nexample syntax is bash check_who_is_that -c xxx.xxx.xxx.xxx where xxx stands for digits 0-255"
  echo -e "#################################\n"
  exit 1 
fi

if [ -z "$ip_provided" ]; then
echo "I did not received the IP address from you. Please provide it now... "
read ip

  if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]];then
    is_ip_valid
    else
    echo "It does not look like a typicall IP adress"
  fi
fi


if [ "$valid_ip" == "true" ]; then
  if [ "$param_flag" == "csv" ]; then
    who_are_you
    echo "$city;$organisation;$originAS"
  else
    who_are_you
    echo "Organisation: $organisation"
    echo "City: $city"
    echo "ASN: $originAS"
  fi
fi

